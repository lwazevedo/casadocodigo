var express = require('../config/express')();
var request = require('supertest')(express);
var DatabaseCleaner = require('database-cleaner');
var dbClear = new DatabaseCleaner('mysql');

describe('#ProdutosController', function () {

	// beforeEach(function (done) {
	// 	//var conn = express.infra.connectionFactory();

	// 	dbClear.clean(express.infra.connectionFactory(), function () {
	// 		done();
	// 	});

	// 	// conn.query("delete from livros", function(ex, result){
	// 	// 	if(!ex){
	// 	// 		done();
	// 	// 	}
	// 	// });
	// });

	afterEach(function (done) {
		
		dbClear.clean(express.infra.connectionFactory(), function () {
			done();
		});
		
	});


	it('#listagem json', function (done) {
		request.get('/produtos')
		.set('Accept', 'application/json')
		.expect('Content-Type',/json/)
		.expect(200,done);

	});

	it('#cadastro de novo produto com dados invalidos', function(done){
		request.post('/produtos')
		.send({titulo:"", descricao:"novo livro"})
		.expect(400,done);
	});

	it('#cadastro de novo produto com dados validos', function(done){
		request.post('/produtos')
		.send({titulo:"titulo", descricao:"novo livro", preco: 20.50})
		.expect(302,done);
	});
});