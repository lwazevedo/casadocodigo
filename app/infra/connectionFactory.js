var mysql = require('mysql');

var connectionMYSQL = function () {


	if(!process.env.NODE_ENV){
		return mysql.createConnection({
			hots: 'localhost',
			user: 'root',
			password: '1234',
			database: 'casadocodigo_nodejs'
		});		
	}

	if(process.env.NODE_ENV == 'test'){
		return mysql.createConnection({
			hots: 'localhost',
			user: 'root',
			password: '1234',
			database: 'casadocodigo_nodejs_test'
		});		
	}

	if (process.env.NODE_ENV == 'production') {
		process.env.CLEARDB_DATABASE_URL = 'mysql://ba6d530ddbae61:62747882@us-cdbr-iron-east-04.cleardb.net/heroku_1674e06f8559540?reconnect=true';
		
		var url = process.env.CLEARDB_DATABASE_URL;
		var grupos = url.match(/mysql:\/\/(.*):(.*)@(.*)\/(.*)\?/);
		return mysql.createConnection({
			host:grupos[3],
			user:grupos[1],
			password:grupos[2],
			database:grupos[4]
		});
	}

}

module.exports = function(){
	return connectionMYSQL;
}
