module.exports = function(app){
	app.get('/promocoes/form', function (req,res) {
		var connection = app.infra.connectionFactory();
		var produtosDao = new app.infra.ProdutosDAO(connection);

		produtosDao.lista(function(err, results) {
			res.render('promocoes/form',{lista:results});
		});

		connection.end();
	});

	 app.post('/promocoes', function(req,res){
        var promocao = req.body;
		// console.log(promocao);
		app.get('io').emit('novaPromocao',promocao);
		res.redirect('/promocoes/form');        
        // req.assert('titulo', 'titulo é obrigatorio').notEmpty();
        // req.assert('preco', 'Formato invalido').isFloat();

        // var erros = req.validationErrors();
        // if(erros){
        //     res.format({
        //         html: function () {
        //             res.status(400).render('produtos/form', {errosValidacao:erros, produto:produto});
        //         },
        //         json: function () {
        //             res.status(400).json(erros);
        //         }
        //     });
            
        //     return;
        // }

       //  var connection = app.infra.connectionFactory();
       //  var produtosDao = new app.infra.ProdutosDAO(connection);
       //  produtosDao.salva(produto,function (erros, resultados) {
       //     //res.render('produtos/lista');
       //     res.redirect('/produtos');
       // });
    });



};